/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package testing_miet;

import testing_miet.domain.CalculatorModel;
import testing_miet.ui.CalculatorView;

public class App {

    public static void main(String[] args) {
        var view = new CalculatorView();
        var model = new CalculatorModel();
        var presenter = new CalculatorPresenter(model, view);
        presenter.start();
    }
}
