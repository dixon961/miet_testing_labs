package testing_miet.domain;

import static testing_miet.domain.CalculatorOperation.*;

public class ExpressionParser {

    public static CalculatorExpression from(String input) {
        var tokens = input.trim().split("\s");
        if (tokens.length == 3) {
            try {
                var leftOperand = Double.parseDouble(tokens[0]);
                var rightOperand = Double.parseDouble(tokens[2]);
                return switch (tokens[1]) {
                    case "+" -> new CalculatorExpression(leftOperand, rightOperand, PLUS);
                    case "-" -> new CalculatorExpression(leftOperand, rightOperand, MINUS);
                    case "/" -> new CalculatorExpression(leftOperand, rightOperand, DIVIDE);
                    case "*" -> new CalculatorExpression(leftOperand, rightOperand, MULTIPLY);
                    default -> throw new IllegalArgumentException("");
                };
            }
            catch (NumberFormatException e) {
                throw new RuntimeException(e);
            }
        }
        else {
            throw new RuntimeException("Cannot parse expression");
        }
    }
}
