package testing_miet.domain;

import javax.annotation.Nonnull;

public class CalculatorModel {

    public CalculatorModel() {
        // nop
    }

    @Nonnull
    public CalculatorResult calculate(CalculatorExpression expression) {
        return switch (expression.operation()) {
            case PLUS -> plus(expression.leftOperand(), expression.rightOperand());
            case MINUS -> minus(expression.leftOperand(), expression.rightOperand());
            case DIVIDE -> divide(expression.leftOperand(), expression.rightOperand());
            case MULTIPLY -> multiply(expression.leftOperand(), expression.rightOperand());
        };
    }

    private CalculatorResult multiply(double leftOperand, double rightOperand) {
        return new CalculatorResult(
                leftOperand * rightOperand,
                null,
                true
        );
    }

    private CalculatorResult divide(double leftOperand, double rightOperand) {
        return rightOperand == 0.0
                ? CalculatorResult.empty()
                : new CalculatorResult(
                leftOperand / rightOperand,
                null,
                true
        );
    }

    private CalculatorResult minus(double leftOperand, double rightOperand) {
        return new CalculatorResult(
                leftOperand - rightOperand,
                null,
                true
        );
    }

    private CalculatorResult plus(double leftOperand, double rightOperand) {
        return new CalculatorResult(
                leftOperand + rightOperand,
                null,
                true
        );
    }
}
