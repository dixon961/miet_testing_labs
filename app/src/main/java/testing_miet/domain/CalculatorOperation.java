package testing_miet.domain;

public enum CalculatorOperation {

    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE;
}
