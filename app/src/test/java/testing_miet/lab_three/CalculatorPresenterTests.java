package testing_miet.lab_three;

import org.junit.Test;
import org.mockito.Mockito;
import testing_miet.CalculatorPresenter;
import testing_miet.domain.CalculatorModel;
import testing_miet.domain.CalculatorResult;
import testing_miet.ui.CalculatorView;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

public class CalculatorPresenterTests {

    private CalculatorModel model = Mockito.mock(CalculatorModel.class);

    private CalculatorView view = new CalculatorView();

    private CalculatorPresenter presenter = new CalculatorPresenter(model, view);

    @Test
    public void shouldCalculate() {
        // given
        given(model.calculate(any())).willReturn(new CalculatorResult(0.0, null, true));
        this.presenter.start();

        // when
        sleep(2);
        this.view.textField().setText("2 + 10");
        sleep(2);
        this.view.button().doClick(500);
        sleep(2);

        // then
        assertThat(view.label().getText()).isEqualTo("0.0");
    }

    private static void sleep(int sec) {
        try {
            Thread.sleep(sec * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
